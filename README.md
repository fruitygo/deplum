# Deplum 🦊

#### Deplum 🦊 is the official Fabriktor's CICD library.

![Deplum Logo](./img/deplum-fabriktor.png)

[![License Badge](https://img.shields.io/gitlab/license/fruitygo/deplum?color=orange)](https://gitlab.com/fruitygo/deplum/-/raw/main/LICENSE)

[![Tag Badge](https://img.shields.io/gitlab/v/tag/fruitygo/deplum?color=gree&sort=date)](https://gitlab.com/fruitygo/deplum/-/tags)
[![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/fruitygo/deplum)](https://gitlab.com/fruitygo/deplum/-/commits/main?ref_type=heads)
[![Maintenance](https://img.shields.io/maintenance/yes/9999)](https://gitlab.com/fruitygo/deplum/-/project_members)

[![GitLab Stars](https://img.shields.io/gitlab/stars/fruitygo/deplum?style=social)](https://gitlab.com/fruitygo/deplum/-/starrers)

## Table of Contents

- [Description](#description)
- [Installation](#installation)
- [Usage](#usage)
- [License](#license)
- [End](#end)

## Description

**Deplum** 🦊 simplifies `GitLab` job reuse across pipelines in Fabriktor's backend.

- 🔗 [Fabriktor Inc.](https://fabriktor.com)

Additional job templates will be added here in the future.

**Currently, Deplum supports the following:**

- `Managing workflow rules globally in Fabriktor namespace`
- `Creating Artifact Registry registries`
- `Pushing Docker images to Google Cloud Artifact Registry`
- `Deploying on Google Cloud Run from Artifact Registry`
- `Performing a curl health check on a specified URL`

## Installation

To use Deplum 🦊 in a GitLab CI/CD setup, simply include it in a .gitlab-ci.yml file as follows:

```yaml
include:
  - project: "fruitygo/deplum"
    ref: main
    file:
      - create-artifact-registry-repository.yml
      - build-push-image-artifact-registry-with-staging.yml
      - deploy-cloud-run-with-staging.yml
      - test-curl.yml
      - ...
```

## Usage

**To use Deplum 🦊 in a GitLab CI/CD setup:**

1. Include the Deplum project in a .gitlab-ci.yml file as shown in the installation section.
2. Configure the jobs and pipelines to reuse Deplum jobs as needed.

## License

Deplum 🦊 is licensed under the [MIT License](https://gitlab.com/fruitygo/deplum/-/raw/main/LICENSE).

## End

![Fabriktor Logo](https://backend.fabriktor.com/filehub/img/gitlab/chubby.gif)
![Fabriktor Logo](https://backend.fabriktor.com/filehub/img/gitlab/screwdriver.gif)
![Fabriktor Logo](https://backend.fabriktor.com/filehub/img/gitlab/key.gif)
![Fabriktor Logo](https://backend.fabriktor.com/filehub/img/gitlab/boss.gif)
